# charmod-norm
Character Model for the World Wide Web: String Matching and Searching

Please review the document at http://www.w3.org/TR/2015/WD-charmod-norm-20151119/.  Comments on the document should be raised using [github issues](https://github.com/w3c/charmod-norm/issues) for this repository.  If you need to use email, send to www-international@w3.org. 

To make it easier to track comments, please raise separate issues or emails for each comment, and point to the section you are commenting on  using a URL for the dated version of the document.

You can view the github version of the document at http://w3c.github.com/charmod-norm. We try to ensure that the TR version and the github version are kept very close.

This document is being developed by the Internationalization Working Group.


##Last-minute Pre-publication edits##

1. make the following changes to the respec file and push to github 

 1. in the SOTD, change the link on "latest dated version in /TR" to point to the location of the document that is about to be published 

 2. change the following to point to the same location
 ```<link rel="canonical" href="http://www.w3.org/TR/2015/WD-charmod-norm-XXXXXXX/"/>```

 3. change previousPublishDate to reflect the date of the last publication 

2. Remove the following from the snapshot of the file that will be published to TR. 
 ```<link rel="canonical" href="http://www.w3.org/TR/2015/WD-charmod-norm-XXXXXXXX/"/>``` 

3. Edit this README file to point to the latest published version.